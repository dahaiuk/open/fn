# dahai-fn 

> Functions to manipulate maps

## Install Java

Install with [maven and others](https://search.maven.org/artifact/uk.dahai.open/fn/0.0.2/jar)

```sh
<dependency>
  <groupId>uk.dahai.open</groupId>
  <artifactId>fn</artifactId>
</dependency>
```

## Usage

TBD 

### Contributing

The project is available on [gitlab](https://gitlab.com/dahaiuk/open) contributions and bug reports 
welcome.

### Author

**Richard Bourner**

* [linkedin](https://www.linkedin.com/in/rickbourner/)

### License

Copyright © 2017, [Richard Bourner](https://gitlab.com/dahailtd).
Released under the [MIT License](LICENSE).