package uk.dahai.open.any;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.Map;


public class SpeedCloneTest
{
    private static final ObjectMapper mapper = Config.mapper.get();

    private final long SPINS = 1000000;


//    static Supplier<Map<Class<?>, Function<Object, Object>>> platformConverters =
//    () -> Map.of( Short.class, o -> ( (Short) o ).longValue(),
//                  Integer.class, o -> ( (Integer) o ).longValue(),
//                  Float.class, o -> ( (Float) o ).doubleValue() );
//
//    static
//    {
//        Any.Builder.setConverters( platformConverters.get() );
//    }

    Any any = Any.bldr()
                 .with( "1", 1 )
                 .with( "2", "this is a big old string that isn't very interesting" )
                 .with( "3", 3.0 )
                 .with( "list-1", "one" )
                 .with( "list-2", "two" )
                 .with( "list-3", "three" )
                 .with( "stuff", "four".getBytes() )
                 //.with( "another", Map.of( "Hello", 1, "hello2", 3, "hello4", 4.0 ) )
                 //.with( "another", Map.of() )
                 .build();


    private static Object deepCopy( Object object ) throws IOException, ClassNotFoundException
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ObjectOutputStream outputStrm = new ObjectOutputStream( outputStream );
        outputStrm.writeObject( object );
        ByteArrayInputStream inputStream = new ByteArrayInputStream( outputStream.toByteArray() );
        ObjectInputStream objInputStream = new ObjectInputStream( inputStream );
        return objInputStream.readObject();
    }

//    @Test
//    public void deepTest() throws IOException, ClassNotFoundException
//    {
//        final var start = System.currentTimeMillis();
//        for ( int i = 0; i < SPINS; i++ )
//            deepCopy( any );
//        final var end = System.currentTimeMillis();
//
//        System.err.println( "deep clone  " + ( end - start ) );
//    }

    @Test
    public void myCloneTest()
    {
        final var start = System.currentTimeMillis();

        for ( int i = 0; i < SPINS; i++ )
        {
            Any b = Any.as( any );
        }

        final var end = System.currentTimeMillis();

        System.err.println( "shallow clone  " + ( end - start ) );
    }

    private static byte[] flyweight( Map<String, Object> other ) throws JsonProcessingException
    {
        return mapper.writeValueAsBytes( other );
    }

    private static Any cloneFromFlyweight( byte[] flyweight ) throws IOException
    {
        return mapper.readValue( flyweight, Any.class );
    }

    @Test
    public void flyweightWithJacksonTest() throws IOException
    {
        final var start = System.currentTimeMillis();
        byte[] flyweight = flyweight( any );
        for ( int i = 0; i < SPINS; i++ )
        {
            var x = cloneFromFlyweight( flyweight );
        }

        final var end = System.currentTimeMillis();

        System.err.println( "flyweight clone  " + ( end - start ) );
    }

    @Test
    public void showOutput()
    {
        var x = Any.bldr().as( any ).build();
        System.err.println( x );
    }


//    @Test
//    public void flyweightWithDeepCopyTest() throws IOException, ClassNotFoundException
//    {
//        final var start = System.currentTimeMillis();
//
//        var outputStream = new ByteArrayOutputStream();
//        var objectOutputStream = new ObjectOutputStream( outputStream );
//        objectOutputStream.writeObject( any );
//        final var flyweight = outputStream.toByteArray();
//
//
//        for ( int i = 0; i < SPINS; i++ )
//        {
//            var inputStream = new ByteArrayInputStream( flyweight );
//            var objInputStream = new ObjectInputStream( inputStream );
//            var x = objInputStream.readObject();
//        }
//
//        final var end = System.currentTimeMillis();
//
//        System.err.println( "flyweight deep copy clone  " + ( end - start ) );
//    }
}
