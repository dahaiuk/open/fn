package uk.dahai.open.any;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static uk.dahai.open.any.AnyFn.emptyAnySupplier;
import static uk.dahai.open.any.AnyFn.in;

public class AnyFnTest
{
    @Test
    public void testEmptyAny()
    {
        assertEquals( 0, AnyFn.EMPTY_ANY.size() );
    }

    @Test
    public void testEmptyAnySupplier()
    {
        // it's empty
        assertEquals( 0, emptyAnySupplier.get().size() );

        // its a new empty document
        assertNotSame( emptyAnySupplier.get(), emptyAnySupplier.get() );
    }

    @Test
    public void testToAnyWith()
    {
        final var value = AnyFn.toAnyWith( "key" ).apply( "value" );

        assertEquals( 1, value.size() );
        assertEquals( "value", value.getString( "key" ) );
    }

    @Test
    public void testToAnyWithBi()
    {
        final var value = AnyFn.toAnyWith( "key-1", "key-2" ).apply( "value-1", "value-2" );

        assertEquals( 2, value.size() );
        assertEquals( "value-1", value.getString( "key-1" ) );
        assertEquals( "value-2", value.getString( "key-2" ) );
    }

    @Test
    public void testIn()
    {
        final var value = Any.bldr().with( "a", 1 ).with( "b", "hi" ).with( "c", 2 ).build();

        assertTrue( in( "a", Set.of( 0, 2, 1 ) ).test( value ) );
        assertFalse( in( "a", Set.of( 0, 2, 3 ) ).test( value ) );
        assertFalse( in( "a", Set.of() ).test( value ) );

        assertFalse( in( "b", Set.of( 1 ) ).test( value ) );
        assertTrue( in( "b", Set.of( "hi" ) ).test( value ) );

    }

}
