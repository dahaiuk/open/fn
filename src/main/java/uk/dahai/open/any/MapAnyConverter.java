package uk.dahai.open.any;

import com.fasterxml.jackson.databind.util.StdConverter;

import java.util.Map;

public class MapAnyConverter extends StdConverter<Map<String, Object>, Any>
{
    @Override
    public Any convert( Map<String, Object> map )
    {
        return Any.bldr().as( map ).build();
    }
}
