package uk.dahai.open.any;

import java.math.BigDecimal;
import java.util.AbstractMap.SimpleEntry;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.function.*;
import java.util.stream.Collectors;

import static java.util.function.Predicate.not;
import static java.util.function.UnaryOperator.identity;
import static java.util.stream.Collectors.joining;

public interface AnyFn
{
    static Function<Any, Boolean> matches( String key, String regex )
    {
        return stringValueOf( key ).andThen( matches( regex ) );
    }
    static Function<String, Boolean> matches( String regex )
    {
        return string -> string.matches( regex );
    }

    static <T> Predicate<T> asPredicate( Function<T, Boolean> fn )
    {
        return fn::apply;
    }

    static <T> Function<T, Boolean> asFunction( Predicate<T> fn )
    {
        return fn::test;
    }

    Any EMPTY_ANY = Any.bldr().build();

    Supplier<Any> emptyAnySupplier = () -> Any.bldr().build();

    static Function<Object, Any> toAnyWith( String key )
    {
        return value -> Any.of( key, value );
    }

    static BiFunction<Object, Object, Any> toAnyWith( String key1, String key2 )
    {
        return ( val1, val2 ) -> Any.bldr()
                                    .with( key1, val1 )
                                    .with( key2, val2 )
                                    .build();
    }

    static Function<Any, Any> toAnyKeyedBy( Function<Any, String> keyExtractor )
    {
        return value -> toAnyWith( keyExtractor.apply( value ) ).apply( value );
    }

    static <T> Function<Any, T> valueOf( String key, Class<T> clazz )
    {
        return any -> clazz.cast( any.get( key ) );
    }

    static Function<Any, Class> classOf( String key )
    {
        return any -> any.get( key ).getClass();
    }

    static <T> Function<Any, T> valueOfOr( String key, T otherwise )
    {
        return ( any ) -> any.get( key, otherwise );
    }

    static Function<Any, String> stringValueOf( String key )
    {
        return any -> any.getString( key );
    }

    static Function<Any, Long> longValueOf( String key )
    {
        return any -> any.getLong( key );
    }

    static Function<Any, Number> numberValueOf( String key )
    {
        return any -> any.getNumber( key );
    }

    static Function<Any, BigDecimal> bigDecimalValueOf( String key )
    {
        return any -> any.getBigDecimal( key );
    }

    static Function<Any, Boolean> boolValueOf( String key )
    {
        return any -> any.getBoolean( key );
    }

    static Function<Any, Any> mergeRight( Any vo )
    {
        return any -> mergeRight.apply( any, vo );
    }

    static Function<Any, Any> mergeLeft( Any vo )
    {
        return any -> mergeLeft.apply( any, vo );
    }

    BiFunction<Any, Any, Any> mergeRight = ( l, r ) -> Any.bldr().as( r ).as( l ).build();

    BiFunction<Any, Any, Any> mergeLeft = ( l, r ) -> mergeRight.apply( r, l );

    static Function<Any, Any> translate( final Map<String, String> pairs )
    {
        return any ->
        {
            final var bldr = Any.bldr();
            any.entrySet().forEach( e -> bldr.with( pairs.getOrDefault( e.getKey(), e.getKey() ), e.getValue() ) );
            return bldr.build();
        };
    }

    static Function<Any, Any> using( String key )
    {
        return valueOfOr( key, EMPTY_ANY );
    }

    static Function<Any, Any> mergeDown( String key )
    {
        return vo ->
        {

            final var top = AnyFn.without( key ).apply( vo );
            final var bottom = using( key ).apply( vo );

            return mergeRight( top ).apply( bottom );
        };
    }

    static Function<Any, Any> mergeUp( String key )
    {
        return vo ->
        {
            final var top = AnyFn.without( key ).apply( vo );
            final var bottom = using( key ).apply( vo );
            return mergeLeft( top ).apply( bottom );
        };
    }

    static <T> Consumer<T> doWhen( Predicate<T> when, Consumer<T> then )
    {
        return any ->
        {
            if ( when.test( any ) )
                then.accept( any );
        };
    }

    static <T> Consumer<T> doWhen( Predicate<T> when, Consumer<T> then, Consumer<T> otherwise )
    {
        return any ->
        {
            if ( when.test( any ) )
                then.accept( any );
            else
                otherwise.accept( any );
        };
    }

    static <T> Function<T, T> when( Predicate<T> when, Function<T, T> then )
    {
        return when( when, then, Function.identity() );
    }

    static <T, U> Function<T, U> when( Predicate<T> when, Function<T, U> then, Function<T, U> otherwise )
    {
        return any ->
        {
            if ( when.test( any ) )
                return then.apply( any );
            else
                return otherwise.apply( any );
        };
    }

    static Function<Object, Any> putValue( String key, Any vo )
    {
        return value -> Any.bldr().as( vo ).with( key, value ).build();
    }

    static Function<Any, Any> put( String key, Object value )
    {
        return any -> Any.bldr().as( any ).with( key, value ).build();
    }


    static Function<Any, Any> put( String key, Function<Any, ?> fn )
    {
        return any -> Any.bldr().as( any ).with( key, fn.apply( any ) ).build();
    }

    static Function<Any, Any> put( String key, Supplier<?> supplier )
    {
        return any -> Any.bldr().as( any ).with( key, supplier.get() ).build();
    }

    static Function<Any, Any> put( String key, Predicate<Any> fn )
    {
        return any -> Any.bldr().as( any ).with( key, fn.test( any ) ).build();
    }

    static <T> Consumer<List<T>> add( T value )
    {
        return ( list ) -> list.add( value );
    }

    static <T> Consumer<Any> with( String key, Consumer<T> action, Class<T> clazz )
    {
        return ( any ) -> action.accept( any.get( key, clazz ) );
    }

    Predicate<Any> isEmpty = Any::isEmpty;
    Predicate<Any> isNotEmpty = isEmpty.negate();

    static Predicate<Any> has( String key )
    {
        return any -> any.containsKey( key );
    }

    static Predicate<Any> hasNot( String key )
    {
        return has( key ).negate();
    }

    static Predicate<Any> sizeIs( int size )
    {
        return any -> any.size() == size;
    }

    static Predicate<Any> hasAll( String... fields )
    {
        return any -> any.keySet().containsAll( Set.of( fields ) );
    }

    static Predicate<Any> in( String key, final Collection values )
    {
        return any -> values.contains( any.get( key ) );
    }

    static Predicate<Any> valuesEqual( String keyA, String keyB )
    {
        return any -> any.get( keyA, new Object() ).equals( any.get( keyB ) );
    }

    static Predicate<Any> is( String key, Object value )
    {
        return any -> value.equals( any.get( key ) );
    }

    static Predicate<Any> isNot( String key, Object value )
    {
        return is( key, value ).negate();
    }

    static Predicate<Any> classIs( String key, Class clazz )
    {
        return any -> clazz.isAssignableFrom( any.get( key ).getClass() );
    }

    static Predicate<Any> listContains( String key, Object value )
    {
        return any -> any.getList( key, value.getClass(), List.of() ).contains( value );
    }

    static Predicate<Any> listDoesNotContain( String key, Object value )
    {
        return not( listContains( key, value ) );
    }

    static Predicate<Any> arrayContains( String key, Object value )
    {
        return any -> Arrays.stream( any.get( key, new Object[ 0 ] ) ).anyMatch( o -> o.equals( value ) );
    }

    static Predicate<Any> arrayContainsAll( String key, Set<Object> values )
    {
        return any -> Arrays.asList( any.get( key, new Object[ 0 ] ) ).containsAll( values );
    }

    static Predicate<Any> arrayContainsDoesNotContainAll( String key, Set<Object> values )
    {
        return not( arrayContainsAll( key, values ) );
    }

    static Predicate<Any> arrayContainsAny( String key, Set<Object> values )
    {
        return any -> Arrays.stream( any.get( key, new Object[ 0 ] ) ).anyMatch( values::contains );
    }

    static Predicate<Any> arrayDoesNotContainAny( String key, Set<Object> values )
    {
        return not( arrayContainsAny( key, values ) );
    }

    static Predicate<Any> arrayDoesNotContain( String key, Object value )
    {
        return not( arrayContains( key, value ) );
    }

    static Predicate<Any> isEmpty( String key )
    {
        return any -> any.get( key, List.of() ).size() == 0;
    }


    static Function<Any, Any> toProjectionOf( String... keys )
    {
        return toProjectionOf( Set.of( keys ) );
    }

    static Function<Any, Any> toProjectionOf( Set<String> keys )
    {
        return toProjectionOf( entry -> keys.contains( entry.getKey() ) );
    }

    static Function<Any, Any> toProjectionOf( Predicate<? super Map.Entry<String, Object>> filter )
    {
        return any ->
        {
            final var bldr = Any.bldr();

            any.entrySet()
               .parallelStream()
               .filter( filter )
               .forEach( entry -> bldr.with( entry.getKey(), entry.getValue() ) );

            return bldr.build();
        };
    }

    static Function<Any, Any> rename( String from, String to )
    {
        if ( from.equals( to ) ) return identity();

        return any -> Any.bldr()
                         .as( any )
                         .with( to, any.get( from ) )
                         .without( from )
                         .build();
    }

    static Consumer<Any> assertThatItHas( String... fields )
    {
        final var msg = "Expected fields  " + Arrays.toString( fields );
        return assertThat( hasAll( fields ), msg );
    }

    static Consumer<Any> assertThat( Predicate<Any> test, String msg )
    {
        return any ->
        {
            assert test.test( any ) : msg;
        };
    }

    static Predicate<Any> hasAny( String... fields )
    {
        final var blacklist = Set.of( fields );
        return any -> any.keySet()
                         .stream()
                         .anyMatch( blacklist::contains );
    }

    static Predicate<Any> hasOnly( String... fields )
    {
        final var whitelist = Set.of( fields );
        return any -> any.keySet()
                         .stream()
                         .filter( not( whitelist::contains ) )
                         .findAny()
                         .isEmpty();
    }

    static Function<Any, String> anyToCsv( String... columns )
    {
        return any -> Arrays.stream( columns )
                            .filter( any::containsKey )
                            .map( key -> any.get( key ).toString() )
                            .map( x -> x.replaceAll( "\"", "" ) ).map( x -> "\"" + x + "\"" )
                            .collect( joining( ",", "", "\n" ) );
    }

    static Function<Any, Any> without( String... keys )
    {
        final var keySet = keys == null ? Set.of() : Set.of( keys );
        return any -> any.entrySet()
                         .stream()
                         .filter( e -> !keySet.contains( e.getKey() ) )
                         .reduce( Any.bldr(), ( a, b ) -> a.with( b.getKey(), b.getValue() ), ( a, b ) -> b )
                         .build();
    }

    static Predicate<Any> isBlank( String key )
    {
        return any -> any.getString( key ) == null || any.getString( key ).trim().equals( "" );
    }

    static <T> Predicate<Any> isSame( String key, Comparable<T> value )
    {
        return any -> value.compareTo( (T) any.get( key ) ) == 0;
    }

    static <T> Predicate<Any> isBefore( String key, Comparable<T> value )
    {
        return any -> value.compareTo( (T) any.get( key ) ) > 0;
    }

    static <T> Predicate<Any> isSameOrBefore( String key, Comparable<T> value )
    {
        return any -> value.compareTo( (T) any.get( key ) ) >= 0;
    }

    static <T> Predicate<Any> isSameOrAfter( String key, Comparable<T> value )
    {
        return any -> value.compareTo( (T) any.get( key ) ) <= 0;
    }

    static Function<Any, Any> toComplementOf( Set<String> keys )
    {
        return any ->
        {
            final var bldr = Any.bldr().as( any );
            keys.forEach( bldr::without );
            return bldr.build();
        };
    }

    Function<Number, Byte> asByte = n -> n == null ? null : n.byteValue();
    Function<Number, Integer> asInteger = n -> n == null ? null : n.intValue();
    Function<Number, Long> asLong = n -> n == null ? null : n.longValue();
    Function<Number, Double> asDouble = n -> n == null ? null : n.doubleValue();

    static Function<Any, Any> schlong( String key )
    {
        return when( has( key ), compute( key, stringValueOf( key ).andThen( Long::valueOf ) ) );
    }


    static Function<Any, Any> toComplementOf( String... keys )
    {
        return any ->
        {
            final var bldr = Any.bldr().as( any );

            Arrays.stream( keys ).forEach( bldr::without );

            return bldr.build();
        };
    }

    Function<Map, Any> toAny = map -> Any.bldr().as( map ).build();

    static Predicate<Any> isGreaterThan( String key, Number value )
    {
        return any -> any.get( key, Number.class ).doubleValue() > value.doubleValue();
    }

    static Predicate<Any> isLessThan( String key, Number value )
    {
        return any -> any.get( key, Number.class ).doubleValue() < value.doubleValue();
    }

    static Predicate<Any> isEqual( String key, Number value )
    {
        return any -> any.get( key, Number.class ).doubleValue() == value.doubleValue();
    }

    static Predicate<Any> isGreaterThanOrEqual( String key, Number value )
    {
        return isGreaterThan( key, value ).or( isEqual( key, value ) );
    }

    static Predicate<Any> isLessThanOrEqual( String key, Number value )
    {
        return isLessThan( key, value ).or( isEqual( key, value ) );
    }

    static <T, E> BiConsumer<T, E> doWhen( BiPredicate<T, E> when, BiConsumer<T, E> then )
    {
        return ( any, obj ) ->
        {
            if ( when.test( any, obj ) )
                then.accept( any, obj );
        };
    }


    static <T, U, V> Function<Any, Any> transform( Predicate<T> matcher, Function<U, V> fn )
    {
        return vo -> Any.as( vo.entrySet()
                               .stream()
                               .map( e -> matcher.test( (T) e.getValue() ) ? new AbstractMap.SimpleEntry<>( e.getKey(), fn.apply( (U) e.getValue() ) )
                                                                           : new AbstractMap.SimpleEntry<>( e.getKey(), e.getValue() ) )
                               .collect( Collectors.toMap( AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue ) ) );
    }

    static <T> Function<Any, Any> compute( String key, Function<Any, T> fn )
    {
        return vo -> Any.bldr()
                        .as( vo )
                        .with( key, fn.apply( vo ) )
                        .build();
    }

    static <T> Function<Any, Any> compute( String key, Supplier<T> supplier )
    {
        return vo -> Any.bldr()
                        .as( vo )
                        .with( key, supplier.get() )
                        .build();
    }

    static <T> Function<Any, Any> compute( String key, Callable<T> callable )
    {
        return vo ->
        {
            try
            {
                return Any.bldr()
                          .as( vo )
                          .with( key, callable.call() )
                          .build();
            }
            catch ( Exception e )
            {
                throw new RuntimeException( e );
            }
        };
    }

    static Function<Any, Any> compute( String key, Predicate<Any> predicate )
    {
        return vo -> Any.bldr()
                        .as( vo )
                        .with( key, predicate.test( vo ) )
                        .build();
    }

}

